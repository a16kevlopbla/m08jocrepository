package com.mygdx.game;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.ui.List;

public class MyGdxGame extends ApplicationAdapter {
	SpriteBatch batch;
	Texture playerImg, enemyImg;
	Rectangle player;
	ArrayList<Rectangle> enemies;
	int enemyX = 0, enemyY = 540;
	float time = 0;
	boolean change = false;

	@Override
	public void create() {
		batch = new SpriteBatch();
		player = new Rectangle(0, 0, 80, 80);
		playerImg = new Texture("player.jpg");
		enemyImg = new Texture("enemigo.jpg");
		enemies = new ArrayList<Rectangle>();
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		update();
		input();

		batch.begin();
		for (Rectangle enemy : enemies) {
			batch.draw(enemyImg, enemy.x, enemy.y, enemy.width, enemy.height);
			enemy.y = enemy.y - 1;
		}
		batch.draw(playerImg, player.x, player.y, player.width, player.height);
		batch.end();
	}

	@Override
	public void dispose() {
		batch.dispose();
		playerImg.dispose();
		enemyImg.dispose();
	}

	private void input() {
		if (Gdx.input.isKeyPressed(Input.Keys.D) | Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
			if (player.x < 740) {
				player.x = player.x + 5;
			}
		}
		if (Gdx.input.isKeyPressed(Input.Keys.A) | Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
			if (player.x > 0) {
				player.x = player.x - 5;
			}
		}
	}

	private void update() {
		float delta = Gdx.graphics.getDeltaTime();
		time = time + delta;
		if (time > 1) {
			int random = (int) (Math.random() * 690);
			enemies.add(new Rectangle(random, enemyY, 50, 50));
			time = 0;
		}

		Iterator<Rectangle> iter = enemies.iterator();
		while (iter.hasNext()) {
			Rectangle enemy = iter.next();
			if (enemy.y < 0) {
				iter.remove();
			}
			if (enemy.overlaps(player)) {
				Gdx.app.exit();
			}
		}
	}
}
